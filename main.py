import random
import argparse
import math
import sys

SEED = str(math.pi)

def get_data(offset, length, encoding="utf-8"):

    random.seed(SEED)

    random.randbytes(offset)

    return random.randbytes(length).decode(encoding)

def to_tuple(data, encoding="utf-8"):

    data = data.encode(encoding)

    random.seed(SEED)

    best = None

    offset = 0

    starts : dict[int, int] = {
            best: 0
    }

    while best is None or starts[best] < len(data):

        byte = random.randbytes(1)

        starts[offset] = 0

        for index in starts:

            if data[starts[index]] == byte:

                starts[index] += 1

            else:

                starts[index] = 0

        starts = { offset: starts[offset] for offset in starts if starts[offset] != 0}

        best = max(starts, key=lambda k: starts[k], default=None)

    return best, starts[best]


def get_cmd(parser):

    parser.add_argument("offset", type=int)

    parser.add_argument("length", type=int)

    parser.set_defaults(func=lambda args: print(get_data(args.offset, args.length)))

def to_cmd(parser):

    parser.set_defaults(func=lambda args: print(to_tuple(sys.stdin.read())))


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers();

    get_parser = subparsers.add_parser("get", help="Get data from tuple")

    get_cmd(get_parser)

    to_parser = subparsers.add_parser("to", help="Transform data to tuple")

    to_cmd(to_parser)

    args = parser.parse_args()

    args.func(args)
